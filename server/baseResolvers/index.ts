import CommentBaseResolver from "./comment.baseResolver";
import MovieBaseResolver from "./movie.baseResolver";

export default [ CommentBaseResolver, MovieBaseResolver ] as const;
